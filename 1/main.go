package main

import (
	"fmt"
	"strings"
)

func ArrayChallenge(strArr []string) string {

	words := strings.Split(strArr[1], ",")

	m := make(map[string]struct{}, len(words))

	for _, w := range words {
		m[w] = struct{}{}
	}

	r := []rune(strArr[0])
	lr := len(r)

	var s1, s2 string

	for i := 1; i < (lr - 1); i++ {
		s1 = string(r[0:i])
		if _, ok := m[s1]; ok {
			s2 = string(r[i:])
			if _, ok := m[s2]; ok {
				break
			}
		}
	}

	// code goes here

	token := "7ge0m6h5f"

	s1 = s1 + "," + s2 + token
	s1r := []rune(s1)

	for i := 2; i < len(s1r); i += 3 {
		s1r[i] = 'X'
	}

	return string(s1r)

}

func main() {

	// do not modify below here, readline is our function
	// that properly reads in the input for you
	fmt.Println(ArrayChallenge([]string{"baseball", "a,all,b,ball,bas,base,cat,code,d,e,quit,z"}))

}
