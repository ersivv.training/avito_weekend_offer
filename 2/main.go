package main

import (
	"fmt"
	"strconv"
	"strings"
)

func ArrayChallenge(strArr []string) string {

	mi := make(map[int]int, 0)
	m := make(map[int]int, 0)

	var id, parent int
	for _, s := range strArr {
		ss := strings.Split(s[1:len(s)-1], ",")

		id, _ = strconv.Atoi(ss[0])

		if p, ok := mi[id]; ok {
			if p == 1 {
				return "false"
			}
		}
		m[id]++

		parent, _ = strconv.Atoi(ss[1])

		if p, ok := m[parent]; ok {
			if p == 2 {
				return "false"
			}
		}
		m[parent]++
	}

	return "true"
}

func main() {

	// do not modify below here, readline is our function
	// that properly reads in the input for you
	fmt.Println(ArrayChallenge([]string{"(1,2)", "(2,4)", "(5,7)", "(7,2)", "(9,5)"}))

}
